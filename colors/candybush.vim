" candy.vim, a Vim color scheme.

set background=dark
highlight clear

if exists("syntax_on")
	syntax reset
endif

let g:colors_name = "candybush"

" Palette
" #878787 102
" #afd7ff 153
" #afffaf 157
" #d7d7ff 189
" #ff87ff 213
" #ffafd7 218
" #ffffd7 230
" #1c1c1c 234
" #262626 235
" #303030 236
"
" #7f200e 1
" #2f7210 2
" #7f5a14 3
" #29237f 4
" #7f2f60 5
" #497f75 6
" #ff411c 9
" #5de21f 10
" #ffb52a 11
" #5247ff 12
" #ff5fc2 13
" #94ffeb 14
"               guifg=        guibg=        ctermfg=      ctermbg=      cterm=        gui=
hi Normal       guifg=white   guibg=#262626 ctermfg=white ctermbg=235
hi Statement    guifg=#afffaf               ctermfg=157                 cterm=none    gui=none
hi Identifier   guifg=fg                    ctermfg=fg                  cterm=none    gui=none
hi Constant     guifg=#afd7ff               ctermfg=153                 cterm=none    gui=none
hi Special      guifg=#afd7ff               ctermfg=153                 cterm=none    gui=none
hi Preproc      guifg=#ffafd7               ctermfg=218                 cterm=none    gui=none
hi Type         guifg=#ffffd7               ctermfg=230                 cterm=none    gui=none

hi Title        guifg=fg                    ctermfg=fg                  cterm=bold    gui=bold

hi Visual       guifg=black   guibg=#d7d7ff ctermfg=black ctermfg=189
hi NonText      guifg=#d7d7ff guibg=bg      ctermfg=189
hi Comment      guifg=#878787               ctermfg=102
hi SpecialKey   guifg=#878787               ctermfg=102

hi LineNr       guifg=#d7d7ff guibg=bg      ctermfg=189   ctermbg=bg
hi CursorLineNr guifg=black   guibg=#d7d7ff ctermfg=black ctermbg=189   cterm=bold

hi Cursor       guifg=black   guibg=white
hi CursorColumn               guibg=#303030               ctermbg=236
hi! link CursorLine CursorColumn

hi StatusLine   guibg=black   guifg=#d7d7ff ctermbg=black ctermfg=189
hi! link StatusLineTerm StatusLine
hi StatusLineNC guibg=white   guifg=#1c1c1c ctermbg=white ctermfg=234
hi! link StatusLineTermNC StatusLineNC

hi TabLine      guifg=#d7d7ff guibg=#262626 ctermfg=189   ctermbg=bg    cterm=none    gui=none
hi TabLineSel   guibg=bg      guifg=#d7d7ff ctermbg=bg    ctermfg=189   cterm=reverse,bold gui=reverse,bold
hi! link TabLineFill TabLine

hi Pmenu        guifg=black   guibg=white   ctermfg=black ctermbg=white
hi PmenuSel     guifg=black   guibg=#d7d7ff ctermfg=black ctermbg=189

hi Wildmenu     guifg=black   guibg=#ffffd7 ctermfg=black ctermbg=230

hi VertSplit    guibg=#d7d7ff guifg=bg      ctermbg=189   ctermfg=bg

hi Search       guifg=black   guibg=#d7d7ff ctermfg=black ctermbg=189

hi Folded       guifg=#d7d7ff guibg=#303030 ctermfg=189   ctermbg=236

hi MatchParen   guifg=bg      guibg=#ff87ff ctermfg=bg    ctermbg=213

hi Todo         guibg=black   guifg=#ffb52a  ctermfg=11    ctermbg=black cterm=reverse gui=reverse
hi ErrorMsg     guibg=white   guifg=#ff411c  ctermfg=9     ctermbg=white cterm=reverse gui=reverse
hi WarningMsg   guifg=#ff411c

hi DiffText     guibg=#ff411c                                            cterm=bold   gui=bold term=reverse

hi SpellBad     guibg=#ff411c
hi SpellCap     guibg=#5247ff
hi SpellRare    guibg=#ff5fc2
hi SpellLocal   guibg=#94ffeb

" TODO add #5de21f
